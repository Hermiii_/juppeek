<?php

session_start();
require_once 'config.php';
require_once 'oauthtwitch.php';
require_once 'conn.php';

?>

<script type="text/JavaScript" defer>
  let a = location.hash; // získání JWT kódu z twitche z URL (twitch odesílá jako fragment nikoliv jako parametr)
  let c = a.indexOf("id_token"); // useknutí přebytečného začátku
  let d = a.indexOf("scope"); // useknutí přebytečného konce
  let b = a.substring(c+9, d-1); // čistý JWT po očistění

  // DEKODOVANI JWT
  let base64Url = b.split('.')[1];
  let base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  let jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));

  let kk = JSON.parse(jsonPayload); // získání čitelného JSONu

  let name = kk.preferred_username; // twitch jméno
  let id = kk.sub; // twitch ID uživatele
  let access_token = kk.at_hash; // twitch access token
  window.location.href="http://localhost/juppeek/callback.php?name=" + name + "&id=" + id + "&l_token=" + access_token; // uložení uživatelovo údajů do URL jako parametry
</script>


<?php

if (!empty($_GET['l_token'])) {
    $_SESSION['token'] = $_GET['l_token'];
}

$oauth->set_headers($_SESSION['token']);

$u_id = $_GET['id'];
$u_name = $_GET['name'];
$a_token = $_SESSION['token'];

if(!empty($u_id) && !empty($u_name) && !empty($a_token)) {

  $result = $conn->query("SELECT id FROM users WHERE t_id =".$u_id);

  if($result->num_rows == 0) {
       $sql = "INSERT INTO users (t_id,t_login,t_token) VALUES ('$u_id','$u_name','$a_token')";
       if ($conn->query($sql) === TRUE) {
         echo "";
       }
  } else {
      $sql = "UPDATE users SET `t_token` = '$a_token', `t_login` = '$u_name' WHERE `t_id` = '$u_id'";
      if ($conn->query($sql) === TRUE) {
        echo "";
      }
  }
}

$_SESSION['user_id'] = $u_id;
$_SESSION['user_name'] = $u_name;

if (!empty($_SESSION['token'])) {
  $stmt = $conn->prepare("SELECT guest FROM users WHERE t_id=".$_SESSION['user_id']);
  $stmt->execute();
  $result = $stmt->get_result();
  $value = $result->fetch_row()[0] ?? false;

  $_SESSION['prd'] = $value;

  if($_SESSION['prd'] === 1) {
    header('Location: admin/');
  }
  elseif ($_SESSION['prd'] === 0) {
    header('Location: user/');
  }
  else {
    header('Location: index.php');
  }

}
$conn->close();
