/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 10.4.20-MariaDB : Database - tipovacka
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tipovacka` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `tipovacka`;

/*Table structure for table `answers` */

DROP TABLE IF EXISTS `answers`;

CREATE TABLE `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_u` int(11) NOT NULL,
  `id_t` int(11) NOT NULL,
  `answer` varchar(6500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;

/*Data for the table `answers` */

insert  into `answers`(`id`,`id_u`,`id_t`,`answer`) values 
(15,88036774,1,'ertert'),
(16,88036774,2,'Ne'),
(17,88036774,3,'sadadasd'),
(18,600317032,2,'ewewr'),
(21,600317032,1,'ss'),
(22,600317032,3,'undefined'),
(23,88036774,5,'qweqsdaas');

/*Table structure for table `tips` */

DROP TABLE IF EXISTS `tips`;

CREATE TABLE `tips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tip` mediumtext NOT NULL,
  `answer` varchar(255) NOT NULL,
  `vypnuto` tinyint(1) NOT NULL DEFAULT 0,
  `moznosti` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

/*Data for the table `tips` */

insert  into `tips`(`id`,`tip`,`answer`,`vypnuto`,`moznosti`) values 
(1,'Skákal pes přes oves? ěščřžýáíé','Občas',1,NULL),
(2,'Je tráva zelená?','Ne',0,NULL),
(3,'Je obloha šedivá?','Ano',1,NULL),
(4,'asdasdqweq','dserwe',0,'fvbcb'),
(5,'sadas','wqeqwe',0,'sds,qweq,qweqsdaas'),
(6,'ssd','wqew',0,'dsdfsfd');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `t_id` int(10) unsigned NOT NULL,
  `t_login` varchar(255) NOT NULL,
  `t_token` varchar(255) NOT NULL,
  `guest` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`t_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `users` */

insert  into `users`(`t_id`,`t_login`,`t_token`,`guest`) values 
(88036774,'hermiii_','qVw7pC4h45ckPD2NuMTwgg',1),
(600317032,'druhahermiii_','Y3qxsSFMURR6ibthTcVP2Q',0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
