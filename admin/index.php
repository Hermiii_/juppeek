<?php

session_start();

if (!isset($_SESSION['user_id']) && !isset($_SESSION['user_name']) && !isset($_SESSION['token'])) {
  header('Location: ../index.php');
}
else {
  if (!empty($_SESSION['token'])) {
    if ($_SESSION['prd'] === 0) {
      header('Location: ../user_panel/index.php');
    }
  }
}

require_once '../theme/functions.php';
require_once '../config.php';
require_once '../oauthtwitch.php';

foreach (glob("components/*.php") as $filename) { require_once $filename; }

head();
navigation();

?>

  <div class="content">
    <div class="data-table content">
      <?php
      
        modalAdd();
        modalEdit();
        modalLos();

      ?>
      
      <div class="container-table100">
			<div class="wrap-table100">
				<div class="table100">
      <table class="main-table" v-if="records.length" style="table-layout:fixed;">
        <thead>
          <tr>
            <th class="column1">&num;</th>
            <th class="column2"></th>
            <th class="column3">Tip</th>
            <th class="column4">Správná odpověď</th>
            <th class="column5">Možné odpovědi</th>
            <th class="column6">Akce</th>
          </tr>
        </thead>
        <tbody>
          <tr
            v-for="(record, i) in records"
            :key="record.id">
            <td class="column1">{{ record.id }}</td>
            <td class="column2"><i class="fas fa-ban" v-if="record.vypnuto == 1"></i></td>
            <td class="column3">{{ record.tip }}</td>
            <td class="column4">{{ record.answer }}</td>
            <td class="column5">{{ record.moznosti }}</td>
            <td class="column6">
              <button @click="deleteRecord(record.id)" class="btn"><i class="fas fa-trash"></i></button>
              <input type="checkbox" @click="offRecord(record.id,record.vypnuto)" v-model="record.vypnuto" v-bind:true-value="0" v-bind:false-value="1">
              <button @click="editRecord(record.id)"  class="btn"><i class="fa-solid fa-pen"></i></button>
            </td>
          </tr>
        </tbody>
      </table>
      </div>
			</div>
		</div>
    </div>
  </div>

  <?php
    topThree();
  ?>

</div>

<?php
  bottomScripts();
?>  
  // other
    records:[],
    isSideActive: false,
    topkaTri:[],

  // draw results
    prvni:'',
    druhy:'',
    treti:'',

  // form
    tip:'',
    answer:'',
    answer_options:'',
    options:'',

  //edit-form
    edit_id:'',
    edit_tip:'',
    edit_answer:'',
    edit_answer_type:'',
    edit_answer_options:'',
    edit_options:'',

    vsichni:[]
  },
  methods: {

    editRow(record) { this.eRecord = record; },
    delRow(record)  { this.dRecord = record; },
    
    <?php

      showModals(); 
      tipStatusOff();
      addTip();
      topkaMetoda();
      drawResults();
      getTip();
      getAllUsers();
      showSidebar();
      delTip();
      editPostTip();
      editTip();

    ?>
  },
  mounted: function() {
    this.getTops(),
    this.getRecords(),
    this.getVsichni()
  }
})
</script>
</body>
</html>
