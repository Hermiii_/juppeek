<?php

function addTip() {
?>
addRecord() {
      let fd = new FormData();

      fd.append('tip', this.tip);
      fd.append('answer', this.answer);
      fd.append('options', this.options);

      axios({
        url: 'assets/insert.php',
        method: 'post',
        data: fd
      })
      .then(res => {
        app.getRecords();

        this.tip = '';
        this.answer = '';
        this.options = '';

        app.hideModal('add');
      })
      .catch(err => {
        console.log(err);
      })
    },

<?php
}