<?php

function modalLos() {
?>
<div>
        <b-modal ref="losRnd" hide-footer class="modal-dialog" id="los-modal">
          <div class="d-block">
            <h3>Losování výherce</h3>
            <table id="winner">
              <tbody>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td id="prvni-text">{{prvni}}</td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td rowspan="3" id="prvni">1.</td>
                  <td id="druhy-text">{{druhy}}</td>
                </tr>
                <tr>
                  <td id="treti-text">{{treti}}</td>
                  <td rowspan="2"  id="druhy">2.</td>
                </tr>
                <tr>
                  <td  id="treti">3.</td>
                </tr>
            </tbody>
            </table>
            <b-button id="draw-button" @click="losovani()">Vylosovat</b-button>
          </div>

        </b-modal>
      </div>
<?php
}