<?php

function editTip() {
?>
editRecord(id) {
      var fd = new FormData();

      fd.append('id', id);

      axios({
        url: 'assets/edit.php',
        method: 'post',
        data: fd
      })
      .then(res => {
        if(res.data.res == 'success') {
          this.edit_id = res.data.row[0];
          this.edit_tip = res.data.row[1];
          this.edit_answer = res.data.row[2];
          this.edit_options = res.data.row[4];

          if(this.edit_options != "") {
            this.edit_answer_options = true;
          }
          app.showModal('edit');
        }
      })
      .catch(err => {
        console.log(err);
      })
    }

<?php
}