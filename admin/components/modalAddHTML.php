<?php
function modalAdd() {
?>
<div>
    <b-modal ref="add" hide-footer class="modal-dialog">
        <div class="d-block">
        <h3>Nový tip</h3>
            <form action="" @submit.prevent="addRecord">
            <div role="group" class="new-tip-form">
            <div class="input-group-prepend">
                <div class="input-group-text">
                Co má uživatel tipovat*:
                </div>
            </div>
            <textarea
                id="tip"
                v-model="tip"
                name="tip"
                type="text"
                placeholder="Tip"
                class="form-control"
                required></textarea>
            </div>
            <div role="group" class="new-tip-form">
            <div class="input-group-prepend">
                <div class="input-group-text">
                Správná odpověď*:
                </div>
            </div>
            <input
                id="answer"
                name="answer"
                type="text"
                placeholder="Správná odpověď"
                class="form-control"
                v-model="answer"
                required />
            </div>
            <div class="new-tip-form">
            <label for="answer_options">Možnosti odpovědí:</label>
            <input type="checkbox" id="answer_options" v-model="answer_options" />
            </div>
            <div role="group" class="" v-if="this.answer_options == true">
            <textarea
                id="options"
                name="options"
                type="text"
                placeholder="Možné odpovědi - oddělit čárkou"
                class="form-control"
                v-model="options"
                required></textarea>
            </div>
            <div id="modal-btns">
            <button type="submit" name="button" class="btn col-5 float-start" id="btn-modal-agree">Přidat</button>
            <b-button class="btn col-5 float-end" @click="hideModal('add')">Zavřít</b-button>
            </div>
        </form>
        </div>

    </b-modal>
</div>
<?php
}