<?php

function delTip() {
?>
  deleteRecord(id) {
    if(window.confirm('Opravdu vymazat?')) {
      var fd = new FormData();

      fd.append('id', id);

      axios({
        url: 'assets/delete.php',
        method: 'post',
        data: fd
      })
      .then(res => {
        if(res.data.res == 'success') {
          app.getRecords();
          alert('Úspěšně smazáno!');
        }
      })
      .catch(err => {
        console.log(err);
      })
    }
  },

<?php
}