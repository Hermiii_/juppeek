<?php

function editPostTip() {
?>
updateRecord() {
        this.edit = false;
        let fd = new FormData();

        fd.append('id', this.edit_id);
        fd.append('tip', this.edit_tip);
        fd.append('answer', this.edit_answer);
        fd.append('options', this.edit_options);

        axios({
          url: 'assets/update.php',
          method: 'post',
          data: fd
        })
        .then(res => {
          if(res.data.res == 'success'){
            app.getRecords();

            this.edit_tip = '';
            this.edit_answer = '';
            this.edit_options = '';

            app.hideModal('edit');
          }
        })
        .catch(err => {
          console.log(err);
        })
      },

<?php
}