<?php

function tipStatusOff() {
?>
offRecord(id,vypnuto){
      let fd = new FormData();
      fd.append('id', id);
      fd.append('vypnuto', vypnuto);

      axios({
        url: 'assets/vypnout.php',
        method: 'post',
        data: fd
      })
      .then(res => {
        if(res.data.res == 'success') {
          app.getRecords();
        }
      })
      .catch(err => {
        console.log(err);
      })
    },

<?php
}
