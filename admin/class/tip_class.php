<?php

require_once '../../oauthtwitch.php';

class Tip_Class{
  private $host = "127.0.0.1";
  private $database_name = "soutez";
  private $username = "root";
  private $password = "";
  private $conn;

  public function __construct(){
    try{
        $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->database_name, $this->username, $this->password);
        $this->conn->exec("set names utf8");
    }catch(PDOException $exception){
        echo "Database could not be connected";
    }
  }
  function getUsers()
  {
      $data = [];
      $id_u = 0;
      $pom = [];
      $sqlQuery = 'SELECT id_u FROM answers
      LEFT JOIN tips ON answers.id_t = tips.id
      WHERE tips.vypnuto = 1 AND NOT answers.answer != tips.answer_t;';

      if ($sql = $this
          ->conn
          ->query($sqlQuery))
      {
          while ($rows = $sql->fetch(PDO::FETCH_ASSOC))
          {
              $pom[] = $rows;
          }
      }

      $idToName = [];
      $k = 0;
      while ($k < sizeof($pom))
      {
          $pom_var = $pom[$k]['id_u'];
          $query = $this
              ->conn
              ->query("SELECT t_id, t_login FROM users WHERE `t_id` = '$pom_var'");
          if ($row = $query->fetch(PDO::FETCH_ASSOC))
          {
              $idToName[$k] = array(
                  $row['t_login']
              );
          }
          $k++;

      }
      $data = $idToName;
      return $data;
  }
 
  function createTip($tip, $answer,$options){
    $sqlQuery = "INSERT INTO tips
                (tip,answer,moznosti)
                VALUES
                (:tip,:answer,:options)";

    $stmt = $this->conn->prepare($sqlQuery);
    if($stmt->execute([":tip"=>$tip, ":answer"=>$answer, ":options"=>$options])){ return true; }
    return false;
}

  public function fetch() {
    $data = [];
    $sqlQuery = "SELECT * FROM tips";

    if ($sql = $this->conn->query($sqlQuery)) {
      while ($rows = $sql->fetch(PDO::FETCH_ASSOC)) {
          $data[] = $rows;
      }
		}
    return $data;
  }

  function delete($id){
    $sqlQuery = "DELETE FROM tips WHERE id = :id";

    $stmt = $this->conn->prepare($sqlQuery);

    if ($stmt->execute([":id"=>$id])) {
      return true;
    }else{
      return false;
    }
  }

    function vypnoutF($id,$vypnuto){
      $sqlQuery = "UPDATE tips SET vypnuto = NOT :vypnuto WHERE id = :id";

      $stmt = $this->conn->prepare($sqlQuery);
      if ($stmt->execute([":id"=>$id, ":vypnuto" => $vypnuto])) {
        return true;
      }else{
        return false;
      }
    }

    function getTop() {
      $data = [];
      $id_u = 0;
      $pom = [];
      $sqlQuery = 'SELECT id_u FROM answers
      LEFT JOIN tips ON answers.id_t = tips.id
      WHERE tips.vypnuto = 1 AND NOT answers.answer != tips.answer_t;';

      if ($sql = $this->conn->query($sqlQuery)) {
        while ($rows = $sql->fetch(PDO::FETCH_ASSOC)) {
            $pom[] = $rows['id_u'];
        }
  		}

      $pom_dup = array_count_values($pom);
      $i = 0;
      $pom_res = [];
      while (current($pom_dup)) {
        $pom_res[$i] = array('id'=>key($pom_dup),'pocet' =>$pom_dup[key($pom_dup)]);
        next($pom_dup);
        $i++;
      }

      $col = array_column($pom_res,'pocet');
      array_multisort($col, SORT_DESC, $pom_res);

      $pom_res2 = [];
      $j = 0;
      
      while($j <3){
        $pom_res2[$j] = $pom_res[$j];
        $j++;
      }
      $idToName=[];
      $k = 0;
      while($k < 3){
        $pom_var = $pom_res2[$k]['id'];
        $query = $this->conn->query("SELECT t_id, t_login FROM users WHERE `t_id` = '$pom_var'");
        if ($row = $query->fetch(PDO::FETCH_ASSOC)) {
          $idToName[$k]= array('name' => $row['t_login'], 'pocet' => $pom_res2[$k]['pocet']);
        }
        $k++;

      }
      $data = $idToName;
      return $data;
    }

    function edit($id){
			$data = [];
			$sqlQuery = "SELECT * FROM tips WHERE id = :id";

			$stmt = $this->conn->prepare($sqlQuery);

      if ($stmt->execute([":id"=>$id])) {
				$row = $stmt->fetch();
				$data = $row;
			}

			return $data;
		}

		function update($id, $tip, $answer, $options){
      $sqlQuery = "UPDATE tips SET tip = :tip, answer = :answer, moznosti = :moznosti WHERE id = :id";

			$stmt = $this->conn->prepare($sqlQuery);

      if ($stmt->execute([":id"=>$id, ":tip"=>$tip, ":answer"=>$answer, ":moznosti"=>$options])) {
				return true;
			}else{
				return false;
			}
		}
}
