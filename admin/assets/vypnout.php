<?php

if (isset($_POST['id'])) {
  $id = $_POST['id'];
  $vypnuto = $_POST['vypnuto'];

  include '../class/tip_class.php';

  $conn = new Tip_Class();

  if($conn->vypnoutF($id,$vypnuto)) {
    $data = array('res' => 'success');
  }
  else {
    $data = array('res' => 'error');
  }

  echo json_encode($data);

}
