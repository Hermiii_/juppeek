<?php
if (isset($_POST['id'])) {
  $id = $_POST['id'];

  include '../class/tip_class.php';

  $conn = new Tip_Class();

  if ($row = $conn->getStatsOne($id)) {
    $data = array('res' => 'success', 'row' => $row);
  }else{
    $data = array('res' => 'error');
  }

  echo json_encode($data);
}
