<?php

include '../class/tip_class.php';

$tip = $_POST['tip'];
$answer = $_POST['answer'];
$options = $_POST['options'];

if (isset($tip) &&
    isset($answer)) {

  $conn = new Tip_Class();

  if($conn -> createTip($tip,$answer,$options)){
      $data = array('res' => 'success');
  }
  else {
    $data = array('res' => 'error');
  }
  echo json_encode($data);
}
