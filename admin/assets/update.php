<?php

$id = $_POST['id'];
$tip = $_POST['tip'];
$answer = $_POST['answer'];
$options = $_POST['options'];

if (isset($tip) &&
    isset($answer)) {
	include '../class/tip_class.php';

	$conn = new Tip_Class();
	if ($conn->update($id, $tip, $answer, $options)) {
		$data = array('res' => 'success');
	}
  else{
		$data = array('res' => 'error');
	}
	echo json_encode($data);
}
