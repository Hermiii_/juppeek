<?php

function head() {
?>
    <!DOCTYPE HTML>
    <html lang="cs">
    <head>
        <meta charset="utf-8" />
        <title>Juppeekova tipovačka</title>
        <link rel="icon" type="image/png" href="/favicon.png"/>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <script src="https://kit.fontawesome.com/930c743b1e.js" crossorigin="anonymous"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-vue/2.21.2/bootstrap-vue.min.css" integrity="sha512-YnP4Ql71idaMB+/ZG38+1adSSQotdqvixQ+dQg8x/IFA4heIj6i0BC31W5T5QUdK1Uuwa01YdqdcT42q+RldAg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" type="text/css" href="../theme/style/style.css" />
        <link rel="stylesheet" type="text/css" href="style/style.css" />
    </head>
    
    <body>
    <div id="app">

<?php
}

function navigation() {
  $backHome;
  if (isset($_SESSION['prd']) == 0) {
    $backHome = "../user/index.php";
  }
  else {
    $backHome = "../admin/index.php";
  }
?>
<nav class="navbar navbar-expand-lg navbar-dark">
<div class="container-fluid">
  <a class="navbar-brand" href="#">Juppeekova tipovačka</a>
  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
        <?php
        if(isset($_SESSION['prd'])
          && $_SERVER['REQUEST_URI'] == "/juppeek/stats/index.php"
          || $_SERVER['REQUEST_URI'] == "/juppeek/stats/") {
        ?>
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="<?php echo $backHome; ?>"><i class="fas fa-home"></i> Domů</a>
          </li>
        <?php
        }
        if(isset($_SESSION['prd'])
          && $_SERVER['REQUEST_URI'] == "/juppeek/admin/index.php"
          || $_SERVER['REQUEST_URI'] == "/juppeek/admin/") {
        ?>
          <li class="nav-item">
            <a class="nav-link" href="../user/index.php" tabindex="-1" aria-disabled="true" ><i class="fa-solid fa-chess-pawn"></i> Uživatelský pohled</a>
          </li>
        <?php
        }
        if(isset($_SESSION['prd'])
          && $_SERVER['REQUEST_URI'] == "/juppeek/user/index.php"
          || $_SERVER['REQUEST_URI'] == "/juppeek/user/") {
          ?>
            <li class="nav-item">
              <a class="nav-link" href="../admin/index.php" tabindex="-1" aria-disabled="true" ><i class="fa-solid fa-chess-pawn"></i> Admin pohled</a>
            </li>
          <?php
          }
        if($_SERVER['REQUEST_URI'] != "/juppeek/stats/index.php"
          && $_SERVER['REQUEST_URI'] != "/juppeek/stats/") {
        ?>
        <li class="nav-item">
          <a class="nav-link" href="../stats/index.php" tabindex="-1" aria-disabled="true"><i class="fa-solid fa-chart-column"></i> Statistiky</a>
        </li>
        <?php
          }
          if(isset($_SESSION['prd'])
            && $_SERVER['REQUEST_URI'] == "/juppeek/admin/index.php"
            || $_SERVER['REQUEST_URI'] == "/juppeek/admin/") {
        ?>
            <b-button id="add-btn" @click="showModal('add')">Přidat nový tip</b-button>
            <b-button id="los-btn" @click="showModal('losRnd')">Losování</b-button>
        <?php
          }
        ?>
      </ul>
  </div>
</div>
</nav>
<?php
}

function bottomScripts() {
?>
<div>
  <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-vue/2.21.2/bootstrap-vue.min.js" charset="utf-8"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js" charset="utf-8"></script>
</div>
<script type="text/javascript">
var app = new Vue({
  el: '#app',
  data: {
<?php
}

function topkaMetoda() {
?>
getTops(){
  axios({
    url: 'assets/topka.php',
    method:'get'
  })
  .then(res => {
    console.log(res);
    this.topkaTri = res.data.rows;
  })
  .catch(err => {
    console.log(err);
  })
},
<?php
}

function topThree() {
  ?>
    <div class="top-players"  :class="{isSide:!isSideActive}">
      <div class="row" style="margin-bottom: 20px" v-for="r in topkaTri">
        <div class="col-2">
          <i class="fas fa-trophy fa-2x"></i>
        </div>
        <div class="col-8">
          {{ r.name }}
        </div>
        <div class="col-2">
          {{r.pocet}}
        </div>
      </div>
      <a class="btn btn-info" id="switch-sticky-sidebar" @click="hideSide">
        <div :class="{'fa-rotate-180':!isSideActive}"><i class="fa-solid fa-arrow-right"></i></div>
      </a>
      <?php
        if($_SERVER['REQUEST_URI'] != "/juppeek/stats/index.php"
        || $_SERVER['REQUEST_URI'] != "/juppeek/stats/") {
      ?>
      <a class="btn btn-info" id="stats-button" href="../stats/index.php">Více statistik</a>
      <?php
        }
      ?>
    </div>
  <?php
  }

function showModals() {
  ?>
    showModal(id) { this.$refs[id].show(); },
    hideModal(id) { this.$refs[id].hide(); },
  <?php
}

function showSidebar() {
  ?>
  hideSide() { this.isSideActive = !this.isSideActive; },
  <?php  
}
