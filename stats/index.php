<?php

session_start();

require '../theme/functions.php';
foreach (glob("assets/get*.php") as $filename) { require_once $filename; }

head();
navigation();
?>
  <div class="content" v-if="pohled == true">
    <div class="data-table content">
      <div class="container-table100">
			<div class="wrap-table100">
				<div class="table100">
          <table class="main-table stats-table" v-if="vsichni.length" style="table-layout:fixed;">
            <thead>
              <tr>
                <th class="column1">Jméno</th>
                <th class="column2">Správně zodpovězeno</th>
                <span class="pohled btn" @click="setPohled">Změnit pohled na: Tipy</span>
              </tr>
            </thead>
            <tbody>
              <tr v-for="r in vsichni">
                <td class="column1">{{ r.name }}</td>
                <td class="column2">{{ r.pocet }}</td>
                <td class="column3"></td>
              </tr>
            </tbody>
          </table>
        </div>
			</div>
		</div>
    </div>
  </div>
  <div class="content" v-else>
    <div class="data-table content">
      <div class="container-table100">
			<div class="wrap-table100">
				<div class="table100">
          <table class="main-table stats-table" v-if="vsichni.length" style="table-layout:fixed;">
            <thead>
              <tr>
                <th class="column1">ID</th>
                <th class="column2">Tip</th>
                <th class="column3">Úspěšnost</th>
                <span class="pohled btn" @click="setPohled">Změnit pohled na: Soutěžící</span>
              </tr>
            </thead>
            <tbody>
              <tr v-for="r in vsechno">
                <td class="column1">{{ r.id }}</td>
                <td class="column2">{{ r.tip }}</td>
                <td class="column3">{{ getUspesnost() }} %</td>
                <td class="column4"></td>
              </tr>
            </tbody>
          </table>
      </div>
			</div>
		</div>
    </div>
  </div>
  
  <?php
    topThree();
  ?>

</div>

<?php
  bottomScripts();
?>
    records:[],
    topkaTri:[],
    isSideActive: false,
    vsichni:[],
    vsechno:[],
    pohled:true
  },
  methods: {
    
    <?php
      
      topkaMetoda();
      showSidebar();
      uspesnost();
      vsichni();
      tipy();

    ?>
    setPohled(){
      this.pohled = !this.pohled;
    }
  },
  mounted() {
    this.getTops(),
    this.getVsichni(),
    this.getTipy()
  }
})
</script>
</body>
</html>
