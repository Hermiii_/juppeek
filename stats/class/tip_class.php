<?php
class Tip_Class
{
    private $host = "127.0.0.1";
    private $database_name = "soutez";
    private $username = "root";
    private $password = "";
    private $conn;

    public function __construct()
    {
        try
        {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->database_name, $this->username, $this->password);
            $this
                ->conn
                ->exec("set names utf8");
        }
        catch(PDOException $exception)
        {
            echo "Database could not be connected.";
        }
    }
    
    public function getTop() {
        $data = [];
        $id_u = 0;
        $pom = [];
        $sqlQuery = 'SELECT id_u FROM answers
        LEFT JOIN tips ON answers.id_t = tips.id
        WHERE tips.vypnuto = 1 AND NOT answers.answer != tips.answer_t;';
  
        if ($sql = $this->conn->query($sqlQuery)) {
          while ($rows = $sql->fetch(PDO::FETCH_ASSOC)) {
              $pom[] = $rows['id_u'];
          }
            }
  
        $pom_dup = array_count_values($pom);
        $i = 0;
        $pom_res = [];
        while (current($pom_dup)) {
          $pom_res[$i] = array('id'=>key($pom_dup),'pocet' =>$pom_dup[key($pom_dup)]);
          next($pom_dup);
          $i++;
        }
  
        $col = array_column($pom_res,'pocet');
        array_multisort($col, SORT_DESC, $pom_res);
  
        $pom_res2 = [];
        $j = 0;
        
        while($j <3){
          $pom_res2[$j] = $pom_res[$j];
          $j++;
        }
        $idToName=[];
        $k = 0;
        while($k < 3){
          $pom_var = $pom_res2[$k]['id'];
          $query = $this->conn->query("SELECT t_id, t_login FROM users WHERE `t_id` = '$pom_var'");
          if ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $idToName[$k]= array('name' => $row['t_login'], 'pocet' => $pom_res2[$k]['pocet']);
          }
          $k++;
  
        }
        $data = $idToName;
        return $data;
      }

    public function getUsers() {
        $data = [];
        $id_u = 0;
        $pom = [];
        $sqlQuery = 'SELECT id_u FROM answers
        LEFT JOIN tips ON answers.id_t = tips.id
        WHERE tips.vypnuto = 1 AND NOT answers.answer != tips.answer_t;';

        if ($sql = $this
            ->conn
            ->query($sqlQuery))
        {
            while ($rows = $sql->fetch(PDO::FETCH_ASSOC))
            {
                $pom[] = $rows['id_u'];
            }
        }

        $pom_dup = array_count_values($pom);
        $i = 0;
        $pom_res = [];
        while (current($pom_dup))
        {
            $pom_res[$i] = array(
                'id' => key($pom_dup) ,
                'pocet' => $pom_dup[key($pom_dup) ]
            );
            next($pom_dup);
            $i++;
        }

        $col = array_column($pom_res, 'pocet');
        array_multisort($col, SORT_DESC, $pom_res);

        $pom_res2 = [];
        $j = 0;

        while ($j < sizeof($pom_res))
        {
            $pom_res2[$j] = $pom_res[$j];
            $j++;
        }
        $idToName = [];
        $k = 0;
        while ($k < sizeof($pom_res2))
        {
            $pom_var = $pom_res2[$k]['id'];
            $query = $this
                ->conn
                ->query("SELECT t_id, t_login FROM users WHERE `t_id` = '$pom_var'");
            if ($row = $query->fetch(PDO::FETCH_ASSOC))
            {
                $idToName[$k] = array(
                    'name' => $row['t_login'],
                    'pocet' => $pom_res2[$k]['pocet']
                );
            }
            $k++;

        }
        $data = $idToName;
        return $data;
    }

    public function getTips(){
        $data = [];
        $pom = [];
        $sqlQuery = 'SELECT tips.tip, tips.id FROM tips ORDER BY tips.id;';

        if ($sql = $this->conn->query($sqlQuery)) {
            while ($rows = $sql->fetch(PDO::FETCH_ASSOC))
            {
                $pom[] = $rows;
            }
        }

        return $pom;
    }

}

