<?php
define('API_LINK', 'https://api.twitch.tv/helix');

/**
 *  Třída OAuthTwitch obsahuje metody a proměnné pro komunikaci s Twitch API typu Helix
 * 
 * @author NoS1gnalWeb - originální kód, Andrea Kozáková (Hermiii_) - úpravy kódu pro potřeby aplikace
 * @license open source
 * 
 */

class OAuthTwitch{
    /**
     * Uživatelské ID, veřejná informace podle které lze dále získávat informace o uživateli
     * @var String client_id
     */
    private $_client_id;
    /**
     * Heslo uživatele pro tuhle aplikaci,
     * @var String client_secret
     */
    private $_client_secret;
    /**
     * Místo kam budou uživatelé přesměrováni po autorizaci
     * @var String redirect_uri
     */
    private $_redirect_uri;
    /**
     * Získání přístupového tokenu, který ověřujte uživatele a povoluje aplikaci, aby jejich jménem zadávala požadavky
     * @var String token
     */
    private $_token;
    /**
     * Posílá token jako hlavičku s typem tokenu
     * @var Array header
     */
    private $_headers = [];
    /**
     * specifikuje, která oprávnění aplikace vyžaduje
     * @var String scope
     */
    private $_scope;

    /**
     * Konstruktor pro tuhle třídu
     *
     * @param string $client_id twitch client id
     * @param string $client_secret twitch client secret
     * @param string $accessToken twitch access token
     *
     * @return void
     */
    public function __construct($client_id, $client_secret, $redirect_uri, $scope){
        $this->_client_id = $client_id;
        $this->_client_secret = $client_secret;
        $this->_redirect_uri = $redirect_uri;
        $this->_scope = $scope;
    }

    /**
     * Seskupuje uživatelská data do jednoho odkazu kvůli žádosti o přístup do aplikace
     *
     * @return String
     */
    public function get_link_connect(){
        $link = "https://id.twitch.tv/oauth2/authorize?response_type=token+id_token&client_id=".$this->_client_id."&redirect_uri=".$this->_redirect_uri."&response_type=code&scope=".$this->_scope." openid".'&claims={"id_token":{"preferred_username":null}}';

        return $link;
    }

    /**
     * Na základě $code získává access token
     *
     * @param String code 30znakový náhodně generovaný řetězec. Který se použije v požadavku na koncový bod tokenu - výměna za přístupový token.
     * @return void
     */
    public function get_token($code){
        $link = "https://id.twitch.tv/oauth2/token?client_id=".$this->_client_id."&client_secret=".$this->_client_secret."&code=".$code."&grant_type=authorization_code&redirect_uri=".$this->_redirect_uri;
        $ch = curl_init($link);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $res = curl_exec($ch);
        curl_close($ch);
        $token = json_decode($res);
        $this->_token = $token;
        return $token->access_token;
    }

    /**
     * Nastavuje hlavičku typem tokenu a uživatelského ID
     * 
     * @param String $token
     */
    public function set_headers($token){
        $this->_headers = [
            'Authorization: Bearer '.$token,
            'Client-Id: '.$this->_client_id
        ];
    }

    /**
     *  Metoda získává informace o kanálu zadaného uživatele podle jeho jedinečného ID
     * 
     * @param String id
     * 
     */
    public function get_channel_info($id){
        $link = API_LINK."/channels?broadcaster_id=".$id;
        
        $ch = curl_init($link);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->_headers);

        $res = curl_exec($ch);
        curl_close($ch);

        return json_decode($res);
    }

}
