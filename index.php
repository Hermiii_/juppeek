<?php

session_start();
require_once 'config.php';
require_once 'oauthtwitch.php';
require_once 'conn.php';

$link = $oauth->get_link_connect();

?>

<!DOCTYPE html>
<html style="height:100%">
<head>
  <meta charset="utf-8" />
  <title>Juppeekova tipovačka</title>
  <link rel="icon" type="image/png" href="/favicon.png"/>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"></head>
  
  <body style="height:100%; background-image: url('img/bg.jpg'); background-repeat: no-repeat; background-size: auto; background-position: cover;">
    <div style="position: relative;height:100%;">
      <h1 style="text-align:center">Juppeekova tipovačka</h1>
      <a href="<?php echo $link; ?>" class="btn" style="color:white;background-color: #52438F;position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);">Login</a>
      <a href="stats/" class="btn" style="color:white;background-color: #52438F;position: absolute;top: 58%;left: 50%;transform: translate(-50%, -50%);">Statistiky</a>
      <footer class="footer" style="position:fixed; bottom:0;text-align:center;">
        <div class="container">
          <p class="text-muted">Code Hermiii_ | Consultation tom_</p>
        </div>
      </footer>
    </div>
  </body>
</html>
