<?php

session_start();

if (!isset($_SESSION['user_id']) && !isset($_SESSION['user_name']) && !isset($_SESSION['access_token'])) {
  header('Location: ../index.php');
}

require '../theme/functions.php';
foreach (glob("components/*.php") as $filename) { require_once $filename; }

head();

navigation();
?>
  <div class="content">
    <div class="data-table content">
      <?php
        modalEditAnswer();
      ?>
      <div class="container-table100">
			<div class="wrap-table100">
				<div class="table100">
      <table class="main-table" v-if="records.length">
        <thead>
          <tr>
            <th class="column1">&num;</th>
            <th class="column2"></th>
            <th class="column3">Tip</th>
            <th class="column4">Tvoje odpověď</th>
            <th class="column6"></th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="(record, i) in records" :key="record.id">
            <td class="column1">{{ record.id }}</td>
            <td class="column2"><i class="fas fa-ban" v-if="record.vypnuto == 1"></i></td>
            <td class="column3">{{ record.tip }}</td>
            <td class="column4">{{ record.answer }}</td>
            <td class="column6">
              <button @click="editRecord(record.id)" class="btn" v-if="record.vypnuto == 0"><i class="fas fa-pen"></i></button>
              <button class="btn" v-if="record.vypnuto == 1" disabled><i class="fas fa-pen"></i></button>
            </td>
          </tr>
        </tbody>
      </table>
      </div>
			</div>
		</div>
    </div>
  </div>
  
  <?php
    topThree();
  ?>

</div>

<?php
  bottomScripts();
?>
    records:[],
    tip:'',
    answer:'',
    options:[],
    edit_id: [],
    edit_tip: '',
    edit_answer: '',
    edit_options: [],
    topkaTri:[],
    isSideActive: false
  },
  methods: {
    
    <?php
      
      topkaMetoda();
      updateAnswer();
      showModals();
      showSidebar();
      editAnswer();

    ?>
    getRecords(){
      axios({
        url: 'assets/read.php',
        method:'get'
      })
      .then(res => {
        this.records = res.data.rows;
        Object.entries(this.records);
        this.records.sort((a, b) => a.id - b.id);

      })
      .catch(err => {})
    }
  },
  mounted() {
    this.getTops(),
    this.getRecords(),
    window.setInterval(() => {
      this.getRecords()
    }, 1000)
  }
})
</script>
</body>
</html>
