<?php

session_start();

class Tip_Class{
  private $host = "127.0.0.1";
  private $database_name = "soutez";
  private $username = "root";
  private $password = "";
  private $conn;

  public function __construct(){
    try{
        $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->database_name, $this->username, $this->password);
        $this->conn->exec("set names utf8");
    }catch(PDOException $exception){
        echo "Database could not be connected: " . $exception->getMessage();
    }
  }

  public function fetch() {
    $data1 = [];
    $pom = $_SESSION['user_id'];
    
    if ($sql = $this->conn->query("SELECT answers.answer, tips.tip, tips.id, tips.vypnuto FROM tips
    LEFT JOIN answers ON tips.id = answers.id_t AND answers.id_u = $pom ORDER BY tips.id;")) {
      while ($rows = $sql->fetch(PDO::FETCH_ASSOC)) {
          $data1[] = $rows;
      }
		}   
    return $data1;
  }

	public function edit($id){
	  $data = array();
    $sqlQuery = "SELECT * FROM tips WHERE id = :id";

    $stmt = $this->conn->prepare($sqlQuery);

    if ($stmt->execute([":id"=>$id])) {
      $row = $stmt->fetch(PDO::FETCH_ASSOC);
      $data= $row;
    }
    $pom = $data['moznosti'].",".$data['answer'];
    $pom_arr = array($data['id'],$data['tip'],$pom);

    return (array)$pom_arr;
	}

	public function update($id, $answer, $u_id){

    $result = $this->conn->prepare("SELECT id_t FROM answers WHERE id_t= :id AND id_u = :u_id");
    $result->execute([":id"=>$id, ":u_id"=>$u_id]);

    if($result->rowCount() != 0) {
      $stmt = $this->conn->prepare("UPDATE answers SET answer = :answer WHERE id_u = :u_id AND id_t= :id AND (SELECT vypnuto FROM tips WHERE id = :id) = 0");
      $stmt->execute([":id"=>$id, ":answer"=>$answer, ":u_id"=>$u_id]);
      return true;
    }
    else {
      $stmt = $this->conn->prepare("SELECT vypnuto FROM tips WHERE id = :id");
      $stmt->execute([":id"=>$id]);
      $off = $stmt->fetch();

      if($off['vypnuto'] == 0) {
        $stmt2 = $this->conn->prepare("INSERT INTO answers (id_u,id_t,answer) VALUES (:u_id,:id,:answer)");
        $stmt2->execute([":id"=>$id, ":answer"=>$answer, ":u_id"=>$u_id]);
        return true;
      }
      else {
        return false;
      }
    }
    return false;
	}

  public function getTop() {
    $data = [];
    $id_u = 0;
    $pom = [];
    $sqlQuery = 'SELECT id_u FROM answers
    LEFT JOIN tips ON answers.id_t = tips.id
    WHERE tips.vypnuto = 1 AND NOT answers.answer != tips.answer_t;';

    if ($sql = $this->conn->query($sqlQuery)) {
      while ($rows = $sql->fetch(PDO::FETCH_ASSOC)) {
          $pom[] = $rows['id_u'];
      }
    }

    $pom_dup = array_count_values($pom);
    $i = 0;
    $pom_res = [];
    while (current($pom_dup)) {
      $pom_res[$i] = array('id'=>key($pom_dup),'pocet' =>$pom_dup[key($pom_dup)]);
      next($pom_dup);
      $i++;
    }

    $col = array_column($pom_res,'pocet');
    array_multisort($col, SORT_DESC, $pom_res);

    $pom_res2 = [];
    $j = 0;
    
    while($j <3){
      $pom_res2[$j] = $pom_res[$j];
      $j++;
    }
    $idToName=[];
    $k = 0;
    while($k < 3){
      $pom_var = $pom_res2[$k]['id'];
      $query = $this->conn->query("SELECT t_id, t_login FROM users WHERE `t_id` = '$pom_var'");
      if ($row = $query->fetch(PDO::FETCH_ASSOC)) {
        $idToName[$k]= array('name' => $row['t_login'], 'pocet' => $pom_res2[$k]['pocet']);
      }
      $k++;

    }
    $data = $idToName;
    return $data;
  }
}


