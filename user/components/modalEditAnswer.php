<?php

function modalEditAnswer(){
?>
<div>
        <b-modal ref="edit" hide-footer class="modal-dialog">
          <div class="d-block">
            <h3>Odpověď na tip</h3>
              <form @submit.prevent="updateRecord">
                <div>
                <div class="input-group-prepend">
                  <div class="input-group-text">
                    Tip:
                  </div>
                </div>
                <textarea
                  id="tip"
                  v-model="edit_tip"
                  name="tip"
                  type="text"
                  class="form-control" disabled></textarea>
              </div>
              <div>
                <div class="input-group-prepend">
                  <div class="input-group-text">
                    Tvoje odpověď*:
                  </div>
                </div>
                <input
                  v-if="this.edit_options.length == 1"
                  id="answer"
                  name="answer"
                  type="text"
                  class="form-control"
                  v-model="answer"
                  required />
                <select v-model="answer" v-if="this.edit_options.length > 1">
                  <option v-for="v in edit_options" v-bind:value="v">
                    {{ v }}
                  </option>
                </select>
              </div>
              <div id="modal-btns">
                <button type="submit" name="button" class="btn btn-sm col-5 float-start" id="btn-modal-agree">Odpověď</button>
                <b-button class="btn btn-sm col-5 float-end" @click="hideModal('edit')">Zavřít</b-button>
              </div>
            </form>
          </div>

        </b-modal>
      </div>
<?php
}
?>