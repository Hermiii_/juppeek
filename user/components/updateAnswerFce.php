<?php

function updateAnswer() {
?>
updateRecord() {
      this.edit = false;
      let fd = new FormData();
      fd.append('id', this.edit_id);
      fd.append('answer', this.answer);
      fd.append('user', '<?php echo $_SESSION['user_id'] ?>');
      axios({
        url: 'assets/update.php',
        method: 'post',
        data: fd
      })
      .then(res => {
        if(res.data.res == 'success'){
          app.getRecords();
          this.edit_tip = '';
          this.answer = '';

          app.hideModal('edit');
        }
      })
      .catch(err => {
        console.log(err);
      })
    },

<?php
}