<?php

function editAnswer() {
?>    
    editRecord(id) {
      var fd = new FormData();
      this.u_answer = '';
      fd.append('id', id);

      axios({
        url: 'assets/edit.php',
        method: 'post',
        data: fd
      })
      .then(res => {
        if(res.data.res == 'success') {
          this.edit_id = res.data.row[0];
          this.edit_tip = res.data.row[1];
          this.options = res.data.row[2];

          let splitOptions = this.options.split(",");
          this.edit_options = splitOptions;
          this.edit_options.sort(() => Math.random() - 0.5);
          
          app.showModal('edit');
        }
      })
      .catch(err => {
        console.log(err);
      })
    },
<?php
}